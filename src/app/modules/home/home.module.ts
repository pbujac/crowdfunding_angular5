import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';

import { homeRouting } from './home.routing';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ArticlesMenuComponent } from './components/articles-menu/articles-menu.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { FinishedProjectsComponent } from './components/finished-projects/finished-projects.component';
import { SingUpFormComponent } from './components/sing-up-form/sing-up-form.component';
import { CategoryHeaderComponent } from './components/category-header/category-header.component';
import { ProjectsPageComponent } from './pages/projects-page/projects-page.component';
import { CampaignComponent } from './components/campaign/campaign.component';
import { ArticleBlogComponent } from './components/article-blog/article-blog.component';
import { CarouselItemComponent } from './components/carousel-item/carousel-item.component';
import { ProductPageComponent } from './pages/product-page/product-page.component';
import { PledgeBoxComponent } from './components/pledge-box/pledge-box.component';
import { ContentProjectBoxComponent } from './components/content-project-box/content-project-box.component';
// import { CommentsComponent } from './components/comments/comments.component';
// import { CommentFormComponent } from './components/comment-form/comment-form.component';

@NgModule({
  imports: [
    CommonModule,
    homeRouting,
    MatPaginatorModule,
    MatTabsModule,
    MatCardModule
  ],
  declarations: [
    HomePageComponent,
    ProjectsPageComponent,
    CarouselComponent,
    ArticlesMenuComponent,
    CategoriesComponent,
    CampaignsComponent,
    StatisticsComponent,
    FinishedProjectsComponent,
    SingUpFormComponent,
    CategoryHeaderComponent,
    ProjectsPageComponent,
    CampaignComponent,
    ArticleBlogComponent,
    CarouselItemComponent,
    ProductPageComponent,
    PledgeBoxComponent,
    ContentProjectBoxComponent
    // CommentsComponent,
    // CommentFormComponent
  ]
})
export class HomeModule {}
