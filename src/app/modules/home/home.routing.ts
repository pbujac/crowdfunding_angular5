import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {ModuleWithProviders} from '@angular/core';
import {ProjectsPageComponent} from './pages/projects-page/projects-page.component';
import {ProductPageComponent} from './pages/product-page/product-page.component';

const HOME_ROUTES: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'projects',
    component: ProjectsPageComponent,
  },
  {
    path: 'product',
    component: ProductPageComponent,
  }
];

export const homeRouting: ModuleWithProviders = RouterModule.forChild(HOME_ROUTES);
