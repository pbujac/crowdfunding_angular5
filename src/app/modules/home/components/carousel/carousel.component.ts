import { AfterViewInit, Component, Input } from '@angular/core';

declare const jQuery: any;

@Component({
  selector: 'app-home-carousel',
  templateUrl: './carousel.component.html'
})
export class CarouselComponent implements AfterViewInit {
  @Input() items: any;

  constructor() {}

  ngAfterViewInit() {
    jQuery('.owl-carousel').owlCarousel({
      items: 1,
      loop: true,
      margin: 10,
      merge: true,
      lazyLoad: true,
      animateOut: 'fadeOut',
      dots: false,
      nav: true,
      navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
      ],
      responsive: {
        678: {
          mergeFit: true
        },
        1000: {
          mergeFit: false
        }
      }
    });
  }
}
