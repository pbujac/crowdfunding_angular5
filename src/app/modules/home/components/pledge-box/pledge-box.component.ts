import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-home-pledge-box',
  templateUrl: './pledge-box.component.html'
})
export class PledgeBoxComponent implements OnInit {

  @Input() pledge: any;

  constructor() {
  }

  ngOnInit() {
  }

}
