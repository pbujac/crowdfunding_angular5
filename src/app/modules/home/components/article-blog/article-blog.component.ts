import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-article-blog',
  templateUrl: './article-blog.component.html'
})
export class ArticleBlogComponent implements OnInit {
  @Input() article: any;
  constructor() {}

  ngOnInit() {}
}
