import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-carousel-item',
  templateUrl: './carousel-item.component.html'
})
export class CarouselItemComponent implements OnInit {
  @Input() item: any;
  constructor() {}

  ngOnInit() {}
}
