import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-campaigns',
  templateUrl: './campaigns.component.html'
})
export class CampaignsComponent implements OnInit {
  @Input() articles: any;

  constructor() {}

  ngOnInit() {}
}
