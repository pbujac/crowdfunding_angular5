import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-category-header',
  templateUrl: './category-header.component.html'
})
export class CategoryHeaderComponent implements OnInit {

  @Input() category: any;

  constructor() { }

  ngOnInit() {
  }

}
