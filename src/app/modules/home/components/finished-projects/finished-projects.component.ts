import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-finished-projects',
  templateUrl: './finished-projects.component.html'
})
export class FinishedProjectsComponent implements OnInit {

  @Input() finishedArticles: any;
  constructor() { }

  ngOnInit() {
  }

}
