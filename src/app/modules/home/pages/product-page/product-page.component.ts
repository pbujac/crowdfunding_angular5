import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html'
})
export class ProductPageComponent implements OnInit {
  public pledges: any;
  constructor() {}

  ngOnInit() {
    this.pledges = [
      {
        sum: 6,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1497250681960-ef046c08a56e?' +
          'auto=format&fit=crop&w=1234&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      },
      {
        sum: 25,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1485963631004-f2f00b1d6606?' +
          'auto=format&fit=crop&w=2468&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      },
      {
        sum: 45,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1475477793686-225275e4c4d9?' +
          'auto=format&fit=crop&w=2550&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      },
      {
        sum: 100,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1469317835793-6d02c2392778?auto=format&' +
          'fit=crop&w=2552&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      },
      {
        sum: 120,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1469317835793-6d02c2392778?auto=format&' +
          'fit=crop&w=2552&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      },
      {
        sum: 140,
        bakers: 22,
        image:
          'https://images.unsplash.com/photo-1469317835793-6d02c2392778?auto=format&' +
          'fit=crop&w=2552&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        description:
          'Save BIG on Runtopia Reach Super Early Bird. FREE US and China Shipping.\n' +
          '      *Runtopia team will reach out to you on shoe size before shipping.',
        delivery: 'Jan 2018'
      }
    ];
  }
}
