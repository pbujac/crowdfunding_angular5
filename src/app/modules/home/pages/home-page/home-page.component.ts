import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent implements OnInit {

  protected articles: any;
  protected campaigns: any;
  protected finishedProjects: any;

  constructor() {

    this.articles = [
      {
        'image': 'https://images.unsplash.com/photo-1477696957384-3b1d731c4cff?auto=format' +
        '&fit=crop&w=2550&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.',
      },
      {
        'image': 'https://images.unsplash.com/photo-1431794062232-2a99a5431c6c' +
        '?auto=format&fit=crop&w=2550&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.',
      },
      {
        'image': 'https://images.unsplash.com/photo-1502021680532-838cfc650323?' +
        'auto=format&fit=crop&w=2551&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.',
      },
      {
        'image': 'https://images.unsplash.com/photo-1502481851512-e9e2529bfbf9?' +
        'auto=format&fit=crop&w=2550&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.',
      }
    ];
    this.finishedProjects = [
      {
        'image': 'https://images.unsplash.com/photo-1502021680532-838cfc650323?' +
        'auto=format&fit=crop&w=2551&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.' +
        'Play every part of the song on an instrument that adapts to you' +
        'Play every part of the song on an instrument that adapts to you'
      },
      {
        'image': 'https://images.unsplash.com/photo-1477696957384-3b1d731c4cff?auto=format' +
        '&fit=crop&w=2550&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D',
        'category': 'Drama',
        'title' : 'Papers that clings to any surface',
        'shortDescription' : 'Play every part of the song on an instrument that adapts to you.' +
        'Play every part of the song on an instrument that adapts to you' +
        'Play every part of the song on an instrument that adapts to you',
      },
    ];
    this.campaigns = [
      {
        'image': 'assets/images/articles/article-8.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription' : 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score' : 90,
        'sum' : 2400
      },
      {
        'image': 'assets/images/articles/article-11.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription' : 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score' : 12,
        'sum' : 12
      },
      {
        'image': 'assets/images/articles/article-12.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription' : 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score' : 34,
        'sum' : 320
      }
    ];
  }

  ngOnInit() {
  }
}
