import {Component, OnInit} from '@angular/core';
import {PageEvent} from '@angular/material';

@Component({
  selector: 'app-projects-page',
  templateUrl: './projects-page.component.html'
})
export class ProjectsPageComponent implements OnInit {

  private campaigns: any;
  private category: any;
  length = 0;
  pageSize = 6;
  pageSizeOptions = [];
  pageEvent: PageEvent;

  constructor() {
  }

  ngOnInit() {
    this.campaigns = [
      {
        'image': 'assets/images/articles/article-8.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 90,
        'sum': 2400
      },
      {
        'image': 'assets/images/articles/article-11.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 12,
        'sum': 12
      },
      {
        'image': 'assets/images/articles/article-12.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 34,
        'sum': 320
      },
      {
        'image': 'assets/images/articles/article-6.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 90,
        'sum': 2400
      },
      {
        'image': 'assets/images/articles/article-4.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 12,
        'sum': 12
      },
      {
        'image': 'assets/images/articles/article-3.jpg',
        'title': 'Silence: Wall of Sound Edition Graphic Novel',
        'category': 'Featured',
        'shortDescription': 'An inspirational tale about a Haitian immigrant who comes to America after the earthquake',
        'author': 'Brwilliant Minds',
        'score': 34,
        'sum': 320
      }
    ];
    this.category = {
      'title' : 'Technology & innovations',
      'description': 'Fund new and groundbreaking projects, including hits from CrowdBeat InDemand'
    };

    this.length = this.campaigns.length;
    this.pageSizeOptions = [this.length - 5, this.length];
  }


  onPaginateChange(setPageSizeOptionsInput) {
    console.log(setPageSizeOptionsInput);
  }

}
