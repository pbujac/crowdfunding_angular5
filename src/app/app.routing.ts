import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {NotFoundComponent} from './shared/not-found.component';
import {SigninComponent} from './signin/signin.component';
import {AuthGuard} from './auth/auth.guard';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './modules/home/home.module#HomeModule'
  },
  {
    path: 'contacts',
    loadChildren: 'app/contacts/contacts.module#ContactsModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'signin', component: SigninComponent
  },
  {
    path: 'not-found', component: NotFoundComponent
  },
  {
    path: '**', redirectTo: 'not-found'
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
