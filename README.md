Corwdfunding - Angular
===========
Crowdfunding client website
## Built With

* [Angular 5](https://angular.io) -  The web framework used
* [Sass](http://sass-lang.com) - CSS Style preprocessor
* [Foundation](http://sass-lang.com) - CSS Style preprocessor

## Run project
Make sure you have installed 
- NodeJS, 
- NPM,
- Angular CLI

 Open project and run in terminal:
```
npm install
ng serve
```
## Screenshots

### Main page
![Alt text](screenshots/img1.png?raw=true "Main Page")
### Campaigns page
![Alt text](screenshots/img2.png?raw=true "Main Page")
### Project page
![Alt text](screenshots/img4.png?raw=true "Article Page")
### Donate page
![Alt text](screenshots/img5.png?raw=true "Dashboard Page")
